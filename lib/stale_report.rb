# encoding: utf-8

require 'awesome_print'
require './energia_client'
require 'logger'
require 'mail'
require 'pony'

start_time = Time.now

$broken_meters = []

def con( message )
	puts message
	$log.info message
end

options = { address: 'smtp.mail.ru', port: 587, domain: 'localhost.localdomain', user_name: 'kssioloop', password: 'm1n5qykQ640y9ZR', authentication: :login, enable_starttls_auto: true }

Mail.defaults { delivery_method :smtp, options }

def send_mail
	Mail.deliver do
		from 'kssioloop@mail.ru'
		to 'report@efl-group.kz'
		subject "Report #{DateTime.now} of broken Energia"
		body "Broken count: #{$broken_meters.size()}"
		add_file 'broken.json'
		add_file 'broken.csv'
	end
end

class EnergiaClient
	attr_reader :indent_level

	def traverse_meters( meter_code, meter_caption, &block )
		@indent_level ||= 0

		sub_meters = list_all_devices( meter_code )[:devices]
		print "#{meter_code}."

		if( sub_meters.size > 0 )
			@indent_level += 1
			meter_id = 1
			puts
			print "#{sub_meters.size} [".rjust(5 + @indent_level)
			sub_meters.each do |sm|
				print "#{meter_id}:#{(sub_meters.size > 5)?((meter_id*1.0/sub_meters.size*100.00).round(2).to_s + '%'):('')} "
				traverse_meters( sm[:attributes][:id], sm[:content], &block )
				meter_id += 1
			end
			print ']'
			@indent_level -= 1
		else
			yield meter_code, meter_caption if block_given?
		end
	end
end

File.open('broken','w'){|f| f.write 'broken'}
$log = Logger.new("stale_report.log", "daily")

client = EnergiaClient.new 
client.keep_alive = true

print "Traversing meters..."
meter_code = 1171 # root Shymkent code
leaves_count = 0
date_start = DateTime.now - 2
date_end = DateTime.now

VALID = 1
INVALID = 0


client.traverse_meters( meter_code, 'Шымкент' ) do |meter, address|
	leaves_count += 1
	print "Reading archive #{meter}..."
	arc = client.get_device_archives( meter, 0, EnergiaClient::DAY_ARCHIVE,  date_start, date_end)
	puts "Done. Archive has #{arc[:params].size} items. "
	device_valid = VALID
	if( arc[:params].size == 0 )
		$broken_meters << {id: meter, address: address}
		device_valid = INVALID
	end
	client.store_device_validity( meter, device_valid )
end
client.logout

con "Leaves count: #{leaves_count}"
con "Broken: #{$broken_meters.size()}"


File.open('broken.json', 'w') {|f| f.write $broken_meters.to_json }

File.open('broken.csv', 'w') do |f|
	f.puts 'no,id,address'
	$broken_meters.each_with_index do |m,i|
		f.puts "#{i},\t#{m[:id]},\t#{m[:address]}"
	end
end

send_mail

puts "Elapssed time #{Time.now - start_time} seconds"

