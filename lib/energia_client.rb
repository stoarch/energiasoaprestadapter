# encoding: utf-8
require 'savon'
require 'json'
require 'sequel'

STATUS_OK = 0
STATUS_UNAVAILABLE = 1
STATUS_INPUTERROR = 2
STATUS_INVALIDSESSION = 3
STATUS_ADMINREQUIRED = 4
STATUS_BUSY = 5
STATUS_NOTFOUND = 6
STATUS_FAIL = 7



class EnergiaClient

	attr_reader :logged_in, :session, :logged_out
	attr_reader :client
	attr_accessor :keep_alive

	def initialize
		@client = Savon.client( wsdl: "http://kz.e-e.ru:1081/wsdl/IRemoteEnergy", log: false, log_level: :debug)
		keep_alive = false # if we need call frequently, this must bet set to true and logged out after operation manually
	end

	def call( operation, message = {} )
		resp = @client.call( operation, message: message )
		
		resp.hash
	end

	def get_operations
		@client.operations
	end

	def login
		resp = call( :login, UserLogin:'Shymkent', Password: '85182q')  
		@session = resp[:envelope][:body][:login_response][:session_key]
		@logged_in = resp[:envelope][:body][:login_response][:return]
		@logged_out = false
	end

	def logout
		resp = call(:logout, SessionKey: @session)
		@logged_out = resp[:envelope][:body][:logout_response][:return]
		@session = nil
		@logged_in = false
	end

	def who_am_i
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:who_am_i, SessionKey: @session) 
		resp_int = unpack( :who_am_i_response, resp )

		logout unless keep_alive

		[resp_int[:user_name],resp_int[:return]]
	end

	def system_info 
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:get_system_info, SessionKey: @session) 
		resp_int = unpack( :get_system_info_response, resp )

		logout unless keep_alive

		[resp_int[:return], resp_int[:xml_result]]
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	def decode_rus( str )
			@ec1 ||= Encoding::Converter.new "UTF-8","Windows-1251",:invalid=>:replace,:undef=>:replace,:replace=>""
			@ec2 ||= Encoding::Converter.new "Windows-1251","UTF-8",:invalid=>:replace,:undef=>:replace,:replace=>""

		@ec2.convert @ec1.convert str
	end

	def decode_rus2( str )
		str.gsub(/\\u([\da-fA-F]{4})/) {|m| [$1].pack("H*").unpack("n*").pack("U*")}
	end

	def decode_rus3( str )
		CGI.unescape str
	end


	attr_reader :enc_table

	def decode_rus4( str )
	@enc_table ||= {
	1072=>"а", 1073=>"б", 1074=>"в", 
	1075=>"г", 1076=>"д", 1077=>"е", 
	1105=>"ё", 1078=>"ж", 1079=>"з", 
	1080=>"и", 1081=>"й", 1082=>"к",
	1083=>"л", 1084=>"м", 1085=>"н", 
	1086=>"о", 1087=>"п", 1088=>"р",
	1089=>"с", 1090=>"т", 1091=>"у", 
	1092=>"ф", 1093=>"х", 1094=>"ц", 
	1095=>"ч", 1096=>"ш", 1097=>"щ",
	1098=>"ъ", 1099=>"ы", 1100=>"ь", 
	1101=>"э", 1102=>"ю", 1103=>"я", 
	1040=>"А", 1041=>"Б", 1042=>"В", 
	1043=>"Г", 1044=>"Д", 1045=>"Е", 
	1025=>"Ё", 1046=>"Ж", 1047=>"З", 
	1048=>"И", 1049=>"Й", 1050=>"К", 
	1051=>"Л", 1052=>"М", 1053=>"Н", 
	1054=>"О", 1055=>"П", 1056=>"Р", 
	1057=>"С", 1058=>"Т", 1059=>"У", 
	1060=>"Ф", 1061=>"Х", 1062=>"Ц", 
	1063=>"Ч", 1064=>"Ш", 1065=>"Щ", 
	1066=>"Ъ", 1067=>"Ы", 1068=>"Ь", 
	1069=>"Э", 1070=>"Ю", 1071=>"Я"
	}

		str.chars.map do |v| 
			ch = @enc_table[v.unpack('U')[0]]
			if ch.nil?
				v
			else
				ch
			end
		end.join
	end

	def list_events_log( device_id = 0, date_start = DateTime.now - 1, date_end = DateTime )
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:get_events_log, SessionKey: @session, DeviceID: device_id , StartTime: date_start, EndTime: date_end )
		resp_int = unpack( :get_events_log_response, resp )

		logout unless keep_alive


		res = []

		if( resp_int[:return].to_i == 0 ) #ok
			res = parse_event_list(resp_int[:xml_result])
		end

		{return: resp_int[:return], events_log: res}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	def list_all_values( device_id = 0)
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:get_values, SessionKey: @session, DeviceID: device_id ) # 0 - all devices 
		resp_int = unpack( :get_values_response, resp )

		logout unless keep_alive


		res = []

		if( resp_int[:return].to_i == 0 ) #ok
			res = parse_values(resp_int[:xml_result])
		end

		{return: resp_int[:return], values: res}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end


	def list_all_events( device_id = 0)
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:get_events, SessionKey: @session, DeviceID: device_id ) # 0 - all devices 
		resp_int = unpack( :get_events_response, resp )

		logout unless keep_alive


		res = []

		if( resp_int[:return].to_i == 0 ) #ok
			res = parse_event_list(resp_int[:xml_result])
		end

		{return: resp_int[:return], events: res}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	def list_all_devices(master_id)
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		res = []
		resp_int = {}
		
		if( stale_cache_for_device?( master_id ) || ( !is_device_cached?( master_id )) )
			resp = call(:list_devices, SessionKey: @session, MasterID: master_id) # 1 - all devices 
			resp_int = unpack( :list_devices_response, resp )

			logout unless keep_alive


			if( resp_int[:return].to_i == 0 ) #ok
				res = parse_devices(resp_int[:xml_result])
			end

			store_devices_in_cache( master_id, res )
		else
			res = read_devices_from_cache( master_id )
			resp_int[:return] = 0
		end
			

		{return: resp_int[:return], devices: res}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	def find_devices_from_cache( partial_name )
		db = connect_to_cache_db

		ds = db[:devices]
		ap ds.where( Sequel.like(Sequel.function(:lower, :content), "%#{partial_name.downcase}%")).sql
		devs = ds.where( Sequel.like(Sequel.function(:lower, :content), "%#{partial_name.downcase}%")).all
		res = map_cache_devs_to_internal( devs )

		{return: 0,devices: res}
	end

	def store_device_validity( meter_id, device_valid )
		db = connect_to_cache_db

		db[:devices].where( id:  meter_id ).update( valid: device_valid )
	end

	def map_cache_devs_to_internal( devs )
		res = devs.map{|d| {content: d[:content], attributes: { id: d[:id], master_id: d[:masterid], type_name: d[:typename], object_name: d[:object_name], location: d[:location], display_name: d[:displayname], valid: d[:valid], is_branch: d[:is_branch]}}}
	end

	def read_stale_devices_from_cache()
		db = connect_to_cache_db()

		ds = db[:devices]
		devs = ds.where(valid: INVALID).all
		res = map_cache_devs_to_internal( devs ) 

		{return: 0,devices: res}
	end

	def read_devices_from_cache( master_id )
		db = connect_to_cache_db()

		ds = db[:devices]
		devs = ds.where(masterid: master_id).all
		res = map_cache_devs_to_internal( devs ) 

		res
	end

	def is_device_cached?( id )
		db = connect_to_cache_db()

		ds = db[:devices]
		ds.where(id: id).count == 1 # we have this device
	end

	def is_branch?( master_id )
		db = connect_to_cache_db()

		ds = db[:devices]
		if ds.where(id: master_id).count == 1 # we have this device
			ds.where(masterid: master_id).count > 0 
		else
			false
		end
	end

	def clear_devices_cache()
		db = connect_to_cache_db
		db[:devices].delete
	end

	def store_devices_in_cache( master_id, devs )
		db = connect_to_cache_db()
		ds = db[:devices]

		master = ds.where(id: master_id).first
		if( master.nil? )
			puts "Meter: #{master_id}"
			ds.insert(id: master_id, masterid: 0, typename: 'type??', objectname: 'obj??', location: 'loc??', displayname: 'disp??', datestored: DateTime.now.strftime("%Y-%m-%d %H:%M:%S.%L"), content: 'con??')
		else
			ds.where(id: master_id).update( datestored: DateTime.now.strftime("%Y-%m-%d %H:%M:%S.%L"), is_branch: devs.size > 0, valid: INDETERMINED )
		end

		devs.each do |dev|
			a = dev[:attributes]
			ds.where(id: a[:id]).delete #if it will slow down, will use update instead
			ds.insert(id: a[:id], masterid: a[:master_id], typename: a[:type_name], objectname: a[:object_name], location: a[:location], displayname: a[:display_name], datestored: DateTime.new(0).strftime("%Y-%m-%d %H:%M:%S.%L"), content: dev[:content], valid: INDETERMINED, is_branch: INDETERMINED)
		end
	end

	def stale_cache_for_device?(master_id)
		res = true #if we have no any items - cache must be filled. Invalidation is good for it.
		db = connect_to_cache_db()
		dev = db[:devices].where(id: master_id).all
		if( dev != nil ) && ( dev != [] ) && ( dev.size > 0 )
			res = dev[0][:datestored] < (DateTime.now-1).strftime("%Y-%m-%d %H:%M:%S.%L")
		end
		res
	end

	def connect_to_cache_db()
		@cache_db ||= Sequel.sqlite('energia.cache.sdb')
	end

	def list_master_devices 
		list_all_devices( 0 ) #root only
	end

	def is_device_onlink(dev_id) 
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:is_device_onlink, SessionKey: @session, DeviceID: dev_id) # 0 - is root devices only
		resp_int = unpack( :is_device_onlink_response, resp )

		logout unless keep_alive

		{return: resp_int[:return], onlink: resp_int[:onlink]}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	HOUR_ARCHIVE =  1
	DAY_ARCHIVE = 2
	MONTH_ARCHIVE = 3

	def get_device_archives(dev_id, param_id, range, start_time, end_time) 
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:get_archives, SessionKey: @session, DeviceID: dev_id, ParamID: param_id, Range: range, StartTime: start_time, EndTime: end_time) # 0 - is root devices only

		resp_int = unpack( :get_archives_response, resp )

		logout unless keep_alive

		res = []

		if( resp_int[:return].to_i == 0 ) #ok
			res = parse_archives_response( resp_int[:xml_result] )
		end

		{return: resp_int[:return], params: res}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	def read_params_from_cache( device_id )
		db = connect_to_cache_db()

		ds = db[:params]
		parms = ds.where(device_id: device_id).all
		res = parms.map{|p| {content: p[:content], attributes: { param_id: p[:id], device_id: p[:device_id], name: p[:name], units: p[:units]}}}

		db.disconnect
		res
	end

	def store_params_in_cache( device_id, params )
		db = connect_to_cache_db()
		ds = db[:params]

		ds.where(device_id: device_id).delete

		params.each do |parm|
			a = parm[:attributes]
			ds.insert(id: a[:param_id], device_id: device_id, name: a[:name], units: a[:units], date_stored: DateTime.now.strftime("%Y-%m-%d %H:%M:%S.%L"), content: parm[:content])
		end

		db.disconnect
	end

	def stale_params_cache?(device_id)
		res = true #if we have no any items - cache must be filled. Invalidation is good for it.
		db = connect_to_cache_db()
		parms = db[:params].where(device_id: device_id).first
		if( parms != nil ) && ( parms != [] ) && ( parms.size > 0 )
			res = parms[:date_stored] < (DateTime.now-7).strftime("%Y-%m-%d %H:%M:%S.%L")
		end
		db.disconnect
		res
	end

	def get_device_params(dev_id) 
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		res = []
		resp_int = {return: 0}
		if( stale_params_cache?( dev_id ) )
			resp = call(:list_params, SessionKey: @session, DeviceID: dev_id) # 0 - is root devices only
			resp_int = unpack( :list_params_response, resp )

			logout unless keep_alive


			if( resp_int[:return].to_i == 0 ) #ok
				res = parse_params_list( resp_int[:xml_result] )
			end
			store_params_in_cache( dev_id, res )
		else
			res = read_params_from_cache( dev_id )
		end

		{return: resp_int[:return], params: res}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	def get_device_config(dev_id) 
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:get_device_config, SessionKey: @session, DeviceID: dev_id) # 0 - is root devices only
		resp_int = unpack( :get_device_config_response, resp )

		logout unless keep_alive

		res = []

		if( resp_int[:return].to_i == 0 ) #ok
			res << parse_device_config( resp_int[:xml_result] )
		end

		{return: resp_int[:return], device: res}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	def get_device(dev_id) 
		if( @session.nil? )
			login
		end
		return "Unknown: Not logged in" if @session.nil?

		resp = call(:get_device, SessionKey: @session, DeviceID: dev_id) # 0 - is root devices only
		resp_int = unpack( :get_device_response, resp )

		logout unless keep_alive

		res = []

		if( resp_int[:return].to_i == 0 ) #ok
			res << parse_one_device( resp_int[:xml_result] )
		end

		{return: resp_int[:return], device: res}
	rescue => detail
		ap detail.backtrace.join("\n")
		ap detail
	end

	def parse_device_config( xml )
		doc = Nokogiri::XML( xml ).css('DeviceConfig')[0]
		child = doc.child
		res = { content: doc.content, name: doc.name }
		child_res = {device_id: 0, name: '', caption: '', kind: ''}
		child_res[:device_id] = child.attributes["DeviceID"].value unless child.attributes["DeviceID"].nil?

		child_res[:name] = child.attributes["Name"].value unless child.attributes["Name"].nil?

		child_res[:caption] = child.attributes["Caption"].value unless child.attributes["Caption"].nil?

		child_res[:kind] = child.attributes["Kind"].value unless child.attributes["Kind"].nil?

		res[:device_config] = child_res
		res

	end

	def parse_one_device( xml )
		doc = Nokogiri::XML( xml )
		fill_device_from_xml( doc.css('Device')[0] )
	end

	def parse_params_list(xml)
		res = []
		doc = Nokogiri::XML(xml)
		params = doc.css('Param')
		params.each do |par|
			par_res = {} 

			par_res = fill_param_from_xml( par )
			res << par_res
		end
		res
	end

	def fill_param_from_xml( param )
			par_res = {}
			par_res[:content] = param.content

			attribs = param.attributes
			param_attribs = {param_id: 0, device_id:0, name: '', units: ''}

			param_attribs[ :param_id ] =  attribs["ID"].value unless attribs["ID"].nil?
			param_attribs[ :device_id ] =  attribs["DeviceID"].value unless attribs["DeviceID"].nil?
			param_attribs[ :name ] =  attribs["Name"].value unless attribs["Name"].nil?
			param_attribs[ :units ] =  attribs["Units"].value unless attribs["Units"].nil?

			par_res[:attributes] = param_attribs
			par_res
	end

	def parse_archives_response(xml)
		res = []
		doc = Nokogiri::XML(xml)
		arc = doc.css('Measure')
		arc.each do |av|
			av_res = {} 

			av_res = fill_arcitem_from_xml( av )
			res << av_res
		end
		res
	end

	def fill_arcitem_from_xml( arcitem )
			av_res = {}
			av_res[:content] = arcitem.content

			attribs = arcitem.attributes
			arcitem_attribs = {param_id:0, kind: 0, range: 0, time_mark: '', time: '', tarif:0, log_time:'', status:0}

			arcitem_attribs[ :param_id ] =  attribs["ParamID"].value unless attribs["ParamID"].nil?
			arcitem_attribs[ :kind ] =  attribs["Kind"].value unless attribs["Kind"].nil?
			arcitem_attribs[ :range ] =  attribs["Range"].value unless attribs["Range"].nil?
			arcitem_attribs[ :time_mark ] =  attribs["Timemark"].value unless attribs["Timemark"].nil?
			arcitem_attribs[ :time ] =  attribs["Time"].value unless attribs["Time"].nil?
			arcitem_attribs[ :tarif ] =  attribs["Tarif"].value unless attribs["Tarif"].nil?
			arcitem_attribs[ :log_time ] =  attribs["LogTime"].value unless attribs["LogTime"].nil?
			arcitem_attribs[ :status ] =  attribs["Status"].value unless attribs["Status"].nil?

			av_res[:attributes] = arcitem_attribs
			av_res
	end

	def parse_event_list(xml)
		res = []
		doc = Nokogiri::XML(xml)
		events = doc.css('Event')
		events.each do |ev|
			ev_res = {} 

			ev_res = fill_event_from_xml( ev )
			res << ev_res
		end
		res
	end

	def fill_event_from_xml( event )
			ev_res = {}
			ev_res[:content] = event.content

			attribs = event.attributes
			event_attribs = {device_id:0, code: 0, cat:0, up:0, val:0, time:''}

			event_attribs[ :device_id ] =  attribs["DeviceID"].value unless attribs["DeviceID"].nil?
			event_attribs[ :code ] =  attribs["Code"].value unless attribs["Code"].nil?
			event_attribs[ :cat ] = attribs["Cat"].value unless attribs["Cat"].nil?
			event_attribs[ :up ] = attribs["Up"].value unless attribs["Up"].nil?
			event_attribs[ :val ] = attribs["Val"].value unless attribs["Val"].nil?
			event_attribs[ :time ] = attribs["Time"].value unless attribs["Time"].nil?

			ev_res[:attributes] = event_attribs
			ev_res
	end

	def parse_values(xml)
		res = []
		doc = Nokogiri::XML(xml)
		values = doc.css('Measure')
		values.each do |val|
			val_res = {} 

			val_res = fill_value_from_xml( val )
			res << val_res
		end
		res
	end

	def fill_value_from_xml( val )
			val_res = {}
			val_res[:content] = val.content

			attribs = val.attributes
			val_attribs = {id:0, time:'', tarif:'', log_time:'', status:''} 
			val_attribs[ :id ] =  attribs["ParamID"].value unless attribs["ParamID"].nil?
			val_attribs[ :time ] =  attribs["Time"].value unless attribs["Time"].nil?
			val_attribs[ :tarif ] = attribs["Tarif"].value unless attribs["Tarif"].nil?
			val_attribs[ :log_time ] = attribs["LogTime"].value unless attribs["LogTime"].nil?
			val_attribs[ :status ] = attribs["Status"].value unless attribs["Status"].nil?

			val_res[:attributes] = val_attribs
			val_res
	end



	def parse_devices(xml)
		res = []
		doc = Nokogiri::XML(xml)
		devices = doc.css('Device')
		devices.each do |dev|
			dev_res = {} 

			dev_res = fill_device_from_xml( dev )
			res << dev_res
		end
		res
	end

	INVALID = 0
	VALID = 1
	INDETERMINED = 2

	def fill_device_from_xml( dev )
			dev_res = {}
			dev_res[:content] = dev.content

			attribs = dev.attributes
			dev_attribs = {id:0, master_id: 0, type_name:'', object_name: '', location: '', display_name: ''}
			dev_attribs[ :id ] =  attribs["ID"].value unless attribs["ID"].nil?
			dev_attribs[ :master_id ] =  attribs["MasterID"].value unless attribs["MasterID"].nil?
			dev_attribs[ :type_name ] = attribs["TypeName"].value unless attribs["TypeName"].nil?
			dev_attribs[ :object_name ] = attribs["ObjectName"].value unless attribs["ObjectName"].nil?
			dev_attribs[ :location ] = attribs["Location"].value unless attribs["Location"].nil?
			dev_attribs[ :display_name ] = attribs["DisplayName"].value unless attribs["DisplayName"].nil?
			dev_attribs[:valid] = INDETERMINED #we do not know already and must check it
			dev_attribs[:is_branch] = INDETERMINED

			dev_res[:attributes] = dev_attribs
			dev_res
	end

	def get_time
		resp = call( :get_time )
		unpack( :get_time_response, resp ) 
	end

	private

	def unpack( res_key, response )
		response[:envelope][:body][res_key]
	end
end
