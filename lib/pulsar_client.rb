# encoding: utf-8

require 'json'
require 'open-uri'
require 'pry'

# Read data from remote service as json and return it as meter
class PulsarClient
	def initialize
	end

	def read_last_vals(date_start)
		res = open("http://46.36.145.7:9193/data?date=#{date_start}").read
		JSON.parse(res)
	end

	def read_last_meters_vals(date)
		pulsar_vals = read_last_vals(date)

		vals = []
		pulsar_vals.each do |v|
			val_res = {}
			val_attribs = {}

			unless( v["DataValue"].nil? and v["DateValue"].nil?)
				date = v["DateValue"]
				date ||= DateTime.now.to_s

				val_res[:content] = v["DataValue"]
				val_res[:attributes] = val_attribs

				val_attribs[:time] = DateTime.parse(date)
				val_attribs[:id] = v["prp_id"]*10000 + v["plc_id"]
				val_attribs[:tarif] = '0'
				val_attribs[:status]='0'
				val_attribs[:log_time] = val_attribs[:time]
			end
			vals << val_res
		end
		{return: 0, values: vals}
	end

	def read_vals(date_start, date_end)
		res = open("http://46.36.145.7:9193/data/between?date_start=#{date_start}&date_end=#{date_end}").read
		JSON.parse(res)
	end

	def read_codes
		res = open("http://46.36.145.7:9193/").read
		vals = JSON.parse(res)
		codes = []
		vals.each do |v|
			code = {}
			code[:content] = v["Name"] + ' ' + v["NameGroup"]
			attrs = {}
			attrs[:id] = v["prp_id"]*10000 + v["plc_id"] 
			attrs[:master_id] = 0
			attrs[:type_name] = 'Расходомер' 
			attrs[:object_name] = ''
			attrs[:location] = ''
			attrs[:display_name] = ''
			attrs[:valid] = 2
			attrs[:is_branch] = 2

			code[:attributes] = attrs

			codes << code
		end
		{return: 0, devices: codes}
	end

	def read_vals_grouped_by_meters(date_start, date_end)
		vals = read_vals(date_start, date_end)
		vals.group_by{|v|v["Name"]}.values
	end

	def read_meters( date_start, date_end )
		vals = read_vals_grouped_by_meters(date_start, date_end)
		meters = []
		vals.each do |v|
			meter = {	counter_id: '?', code: 0, value: 0.0, datetime: nil, caption: '?', factor: 0.01, values: []}

			if( v.size > 0 )
				meter_caption = v[0]["Name"]
				meter[:caption] = meter_caption

				v.each do |mv|
				binding.pry if mv["DataValue"].nil?
					unless( mv["DataValue"].nil? and mv["DateValue"].nil?)
						date = mv["DateValue"]
						date ||= DateTime.now.to_s
						meter[:values] << {read: mv["DataValue"], tbrtime: DateTime.parse(date)}  	
					end
				end
			end
			meters << meter
		end
		meters
	end
=begin 
FROM SERVICE
=> [[{"DateValue"=>"2016-05-03 12:05:45 +0600",
   "Name"=>
    "\u0414\u0435\u0442\u0441\u043A\u0438\u0439 \u0441\u0430\u0434 \u0421\u0430\
u043C\u0430\u043B",
   "typ_arh"=>0,
   "DataValue"=>2.0},
  {"DateValue"=>"2016-05-03 12:08:10 +0600",
   "Name"=>
    "\u0414\u0435\u0442\u0441\u043A\u0438\u0439 \u0441\u0430\u0434 \u0421\u0430\
u043C\u0430\u043B",
   "typ_arh"=>0,
   "DataValue"=>2.0},


FROM DB:
=> [{:counter_id=>"F0001214",
  :code=>770525,
  :value=>0.0,
  :datetime=>nil,
  :caption=>
   "\u0421\u043F\u043E\u0440\u0442 \u043A\u043E\u043C\u043F\u043B\u0435\u043A\u0
441 Petro Kaz - \u0443\u043B. \u0410\u0431\u0430\u044F \u0431.\u043D.",
  :factor=>0.01,
  :values=>
   [{:read=>1391.56, :tbrtime=>2016-05-05 18:00:50 +0600},
=end

end
