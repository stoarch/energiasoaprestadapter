# encoding: utf-8

require 'bundler/setup'
require 'awesome_print'
require 'sinatra'
require 'sinatra/base'
require 'sinatra/contrib'
require 'sinatra/reloader' if development?
require './energia_client'
require './pulsar_client'


$client = EnergiaClient.new
$client.keep_alive = false 

$pulsar_client = PulsarClient.new

class RestAdapter < Sinatra::Base
	configure :development do
		register Sinatra::Reloader
	end

	configure do
		set :server, 'thin'

		set :environment, :development
		set :show_exceptions, true
		set :dump_errors, true
	end

	before do
		expires 500, :public, :must_revalidate
	end

## ROUTES ##
############

	# SOAP dups #
	get '/server-time' do
		$client.get_time.to_json 
	end

	get '/who-am-i' do
		$client.who_am_i
	end

	get '/logout' do
		$client.logout
	end

	get '/system-info' do
		response['Content-type'] = 'text/json; charset=utf-8'
		$client.system_info.to_json
	end

	get '/list-master-devices' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_master_devices.to_json
	end

	get '/list-all-devices' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_all_devices(1171).to_json
	end

	get '/list-devices' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_all_devices(params[:id].to_i).to_json
	end

	get '/list-all-events' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_all_events.to_json
	end

	get '/list-events' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_all_events( params[:code] ).to_json
	end

	get '/list-events-log' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_events_log( params[:code], DateTime.parse(params[:date_start]), DateTime.parse(params[:date_end]) ).to_json
	end

	get '/list-archives' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.get_device_archives( params[:code], params[:param_id], params[:range], DateTime.parse(params[:date_start]), DateTime.parse(params[:date_end]) ).to_json
	end

	get '/operations' do
		response['Content-type'] = 'text/json; charset=utf-8'
		$client.get_operations().to_json
	end

	get '/get-device'do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.get_device(params[:code]).to_json
	end

	get '/get-device-config' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.get_device_config(params[:code]).to_json
	end

	get '/is-device-onlink' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.is_device_onlink(params[:code]).to_json
	end

	get '/get-device-values' do
		response['Content-Type'] = 'text/json; charset=utf-8'
		vals = $client.list_all_values(params[:code]).to_json
	end

	get '/get-device-params' do
		response['Content-Type'] = 'text/json; charset=utf-8'
		$client.get_device_params(params[:code]).to_json
	end

# REST part #
	get '/devices/:code/cur-values' do
		last_modified DateTime.now
		response['Content-Type'] = 'text/json; charset=utf-8'
		vals = $client.list_all_values(params[:code]).to_json
	end

	get '/pulsar/devices' do
		last_modified DateTime.now
		response['Content-Type'] = 'text/json; charset=utf-8'
		vals = $pulsar_client.read_codes().to_json
	end

	get '/pulsar/devices/values/:date' do
		last_modified DateTime.now
		response['Content-Type'] = 'text/json; charset=utf-8'
		vals = $pulsar_client.read_last_meters_vals(params[:date]).to_json
	end

	get '/info/devices/roots/:root_code/childs' do
		last_modified DateTime.now
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_all_devices(params[:root_code]).to_json
	end

	get '/info/devices/roots' do
		cache_control :public 
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_master_devices.to_json
	end

	get '/info/devices/:code' do
		cache_control :public 
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.get_device( params[:code] ).to_json
	end

	get '/info/devices/:code/config' do
		cache_control :public 
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.get_device_config( params[:code] ).to_json
	end

	get '/info/devices/:code/params' do
		cache_control :public
		response['Content-Type'] = 'text/json; charset=utf-8'
		$client.get_device_params(params[:code]).to_json
	end

	get '/info/devices/:code/onlink' do
		cache_control :public 
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.is_device_onlink( params[:code] ).to_json
	end

	get '/info/events' do
		cache_control :public 
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_all_events.to_json
	end

	get '/info/events/:code' do
		cache_control :public 
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_all_events( params[:code] ).to_json
	end

	get '/info/events-log/:code/:date_start/:date_end' do
		cache_control :public 
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.list_events_log( params[:code], DateTime.parse(params[:date_start]), DateTime.parse(params[:date_end]) ).to_json
	end

	get '/devices/stale' do
		response['Content-type'] = 'text/json; charset=utf-8'
		$client.read_stale_devices_from_cache().to_json
	end

	get '/devices/:code/params/:param_id/archives/day/:date_start/:date_end' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.get_device_archives( params[:code], params[:param_id], EnergiaClient::DAY_ARCHIVE, DateTime.parse(params[:date_start]), DateTime.parse(params[:date_end]) ).to_json
	end

	get '/devices/:code/params/:param_id/archives/month/:date_start/:date_end' do
		response['Content-type'] = 'text/json; charset=utf-8'
		devs = $client.get_device_archives( params[:code], params[:param_id], EnergiaClient::MONTH_ARCHIVE, DateTime.parse(params[:date_start]), DateTime.parse(params[:date_end]) ).to_json
	end

	get '/find/device/partial-name/:partial_name' do
		response['Content-type'] = 'text/json; charset=utf-8'
		ap params
		$client.find_devices_from_cache( params[:partial_name] ).to_json
	end


end
